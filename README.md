# external-provisioner

This library is a rewrite of [sig-storage-lib-external-provisioner][1] using
[controller-runtime][2] as backend.

[1]: https://github.com/kubernetes-sigs/sig-storage-lib-external-provisioner/
[2]: https://github.com/kubernetes-sigs/controller-runtime

## Contributing

This library is [Free Software](LICENSE) and every contributions are welcome.

Please note that this project is released with a [Contributor Code of
Conduct](CODE_OF_CONDUCT.md). By participating in this project you agree to
abide by its terms.
